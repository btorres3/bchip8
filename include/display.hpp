#pragma once
#if defined(__APPLE__) || defined(__EMSCRIPTEN__) || defined(_WIN32)
#include <SDL.h>
#elif defined(__linux__)
#include <SDL2/SDL.h>
#endif

#include "./cpu.hpp"

constexpr int WINDOW_WIDTH = 64;
constexpr int WINDOW_HEIGHT = 32;
constexpr int SCALE = 16;

class Display {
 private:
  SDL_Window* window = nullptr;
  SDL_Renderer* renderer = nullptr;
  SDL_Texture* texture = nullptr;
  SDL_Event event;

 public:
  Display();
  ~Display();
  void draw(std::array<std::uint32_t, WINDOW_HEIGHT * WINDOW_WIDTH>& screen);
  int key_press(std::array<std::uint8_t, KEYS>& keypad);
};
