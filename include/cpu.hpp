#pragma once
#include <memory>
#include <string>

#include "./registers.hpp"

constexpr int FONTS = 80;
constexpr int WIDTH = 64;
constexpr int HEIGHT = 32;
constexpr int KEYS = 16;
constexpr int MEMORY = 4096;
constexpr std::array<std::uint8_t, FONTS> FONTSET = {
    0xF0, 0x90, 0x90, 0x90, 0xF0,   // 0
    0x20, 0x60, 0x20, 0x20, 0x70,   // 1
    0xF0, 0x10, 0xF0, 0x80, 0xF0,   // 2
    0xF0, 0x10, 0xF0, 0x10, 0xF0,   // 3
    0x90, 0x90, 0xF0, 0x10, 0x10,   // 4
    0xF0, 0x80, 0xF0, 0x10, 0xF0,   // 5
    0xF0, 0x80, 0xF0, 0x90, 0xF0,   // 6
    0xF0, 0x10, 0x20, 0x40, 0x40,   // 7
    0xF0, 0x90, 0xF0, 0x90, 0xF0,   // 8
    0xF0, 0x90, 0xF0, 0x10, 0xF0,   // 9
    0xF0, 0x90, 0xF0, 0x90, 0x90,   // A
    0xE0, 0x90, 0xE0, 0x90, 0xE0,   // B
    0xF0, 0x80, 0x80, 0x80, 0xF0,   // C
    0xE0, 0x90, 0x90, 0x90, 0xE0,   // D
    0xF0, 0x80, 0xF0, 0x80, 0xF0,   // E
    0xF0, 0x80, 0xF0, 0x80, 0x80};  // F

class Chip8 {
 public:
  Chip8();
  Chip8(std::string game);
  Chip8(std::string game, int n);
  ~Chip8();

  auto get_draw() const -> const bool&;
  auto get_step() const -> const int&;

  void set_draw(bool draw);
  void set_step(int step);
  void set_game(std::string rom);

  void run();

  std::array<std::uint8_t, KEYS> keypad;
  std::array<std::uint32_t, WIDTH * HEIGHT> display;

 private:
  std::string rom;
  std::array<std::uint8_t, MEMORY> memory;
  std::unique_ptr<Registers> regs;
  bool draw;
  int step;
  void reset();
  void cycle();

  // Opcodes
  void exec_00E0();
  void exec_00EE();
  void exec_1NNN(std::uint16_t nnn);
  void exec_2NNN(std::uint16_t nnn);
  void exec_3XNN(std::size_t x, std::uint8_t nn);
  void exec_4XNN(std::size_t x, std::uint8_t nn);
  void exec_5XY0(std::size_t x, std::size_t y);
  void exec_6XNN(std::size_t x, std::uint8_t nn);
  void exec_7XNN(std::size_t x, std::uint8_t nn);
  void exec_8XY0(std::size_t x, std::size_t y);
  void exec_8XY1(std::size_t x, std::size_t y);
  void exec_8XY2(std::size_t x, std::size_t y);
  void exec_8XY3(std::size_t x, std::size_t y);
  void exec_8XY4(std::size_t x, std::size_t y);
  void exec_8XY5(std::size_t x, std::size_t y);
  void exec_8XY6(std::size_t x);
  void exec_8XY7(std::size_t x, std::size_t y);
  void exec_8XYE(std::size_t x);
  void exec_9XY0(std::size_t x, std::size_t y);
  void exec_ANNN(std::uint16_t nnn);
  void exec_BNNN(std::uint16_t nnn);
  void exec_CXNN(std::size_t x, std::uint8_t nn);
  void exec_DXYN(std::size_t x, std::size_t y, std::size_t n);
  void exec_EX9E(std::size_t x);
  void exec_EXA1(std::size_t x);
  void exec_FX07(std::size_t x);
  void exec_FX0A(std::size_t x);
  void exec_FX15(std::size_t x);
  void exec_FX18(std::size_t x);
  void exec_FX1E(std::size_t x);
  void exec_FX29(std::size_t x);
  void exec_FX33(std::size_t x);
  void exec_FX55(std::size_t x);
  void exec_FX65(std::size_t x);
};
