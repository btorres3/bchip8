#pragma once
#include <array>
#include <cstddef>
#include <cstdint>

constexpr int V_REGS = 16;
constexpr int STACK_LVLS = 16;

class Registers {
 public:
  Registers();
  ~Registers();

  void reset();

  std::array<std::uint8_t, V_REGS> v;
  std::uint8_t dt;
  std::uint8_t st;

  std::uint16_t pc;
  std::uint16_t i;
  std::array<std::uint16_t, STACK_LVLS> stack;

  std::size_t sp;
};
