# bCHIP-8

CHIP-8 emulator written in C++, can be run natively or in the web browser using emscripten.

## Getting Started

To get started, you need to have the following dependencies installed:
- cmake
- emscripten
- sdl2

On macOS, you will additionally need to have the command line tools installed. I highly recommend installing the brew package manager if you have not already, as it makes life significantly easier. You can find the installation instructions for brew [here](https://brew.sh/).

Below is how to get started for macOS users. This assumes brew is installed.
```bash
xcode-select --install
brew install cmake emscripten sdl2
git clone https://gitlab.com/btorres3/bchip8.git
```

On Linux distributions, the process looks very similar. Nearly every major Linux distribution should have these packages in their respective package manager. Below is the one-liner command an Ubuntu user can enter to install all the dependencies, and another command to clone the repository. 
```bash
sudo apt install cmake emscripten libsdl2-dev
git clone https://gitlab.com/btorres3/bchip8.git
```

### How to run:
#### Native

Once you have cloned the repository, enter the root directory and create a build directory, go into that directory, and run cmake. Afterwards, you can run make to create the executable.
```bash
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make
```

Afterwards, you can run the emulator (assuming you're still in the build directory) via:
```bash
../bin/bchip8 ../roms/<game>
```
See the [ROMs](#roms) section for more information about ROMs.

#### Emscripten 

Similarly, create and enter the build directory, but invoke emcmake when you run cmake. 
```bash
mkdir build
cd build
emcmake cmake -DCMAKE_BUILD_TYPE=Release ..
make
```

The executable for emscripten lies in public/ instead of bin/. Change directory into public/ and run Python's http server module. The default port is 8000, so you can go to localhost:8000/bchip8.html in your web browser to run the emulator.
```bash
cd ../public
python3 -m http.server
```

#### Compiling with another compiler

If you prefer to use another compiler other than your system's default, you can run:
```bash
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_COMPILER=/path/to/compiler ..
```

This applies for only native, you should not run this with emscripten as the compiler has been set to em++.

#### Debugging

Run: `cmake -DCMAKE_BUILD_TYPE=Debug ..` if you want to inspect and debug the code. Code can be debugged with either lldb or gdb.

This applies for both native and emscripten.

## ROMs

ROMs are not provided. Create a directory called roms/ (in the root folder) and store roms into that folder.
I recommend downloading ROMs from [here](https://github.com/loktar00/chip8/tree/master/roms).

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
