#include "../include/cpu.hpp"

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <vector>

#ifdef __EMSCRIPTEN__
#include <emscripten/emscripten.h>
#endif

[[noreturn]] static inline void prg_exit() {
#ifdef __EMSCRIPTEN__
  emscripten_force_exit(EXIT_FAILURE);
#else
  exit(EXIT_FAILURE);
#endif
}

static inline void read_file_into_mem(const char *file,
                                      std::array<std::uint8_t, MEMORY> &mem) {
  std::ifstream is(file, std::ios::binary);
  if (is.is_open()) {
    is.seekg(0, std::ios_base::end);
    auto size = is.tellg();
    is.seekg(0, std::ios_base::beg);
    is.read((char *)&mem[0x200], size);
  } else {
    std::cerr << "Game not found!" << std::endl;
    prg_exit();
  }

  is.close();
}

Chip8::Chip8()
    : keypad(),
      display(),
      rom(std::string()),
      memory(),
      regs(std::make_unique<Registers>()),
      draw(true),
      step(15) {
  std::copy(FONTSET.begin(), FONTSET.end(), memory.begin());
}

Chip8::Chip8(std::string game)
    : keypad(),
      display(),
      rom(game),
      memory(),
      regs(std::make_unique<Registers>()),
      draw(true),
      step(15) {
  std::copy(FONTSET.begin(), FONTSET.end(), memory.begin());
  read_file_into_mem(rom.c_str(), memory);
}

Chip8::Chip8(std::string game, int n)
    : keypad(),
      display(),
      rom(game),
      memory(),
      regs(std::make_unique<Registers>()),
      draw(true),
      step(n) {
  std::copy(FONTSET.begin(), FONTSET.end(), memory.begin());
  read_file_into_mem(rom.c_str(), memory);
}

Chip8::~Chip8() {}

void Chip8::run() {
  for (int i = 0; i < step; i++) cycle();
}

auto Chip8::get_draw() const -> const bool & { return draw; }

auto Chip8::get_step() const -> const int & { return step; }

void Chip8::set_draw(bool draw) { this->draw = draw; }

void Chip8::set_step(int step) { this->step = step; }

void Chip8::set_game(std::string rom) {
  reset();
  this->rom = rom;
  read_file_into_mem(this->rom.c_str(), memory);
}

void Chip8::reset() {
  std::fill(std::begin(keypad), std::end(keypad), 0);
  std::fill(std::begin(display), std::end(display), 0);
  std::fill(std::begin(memory) + 0x80UL, std::end(memory), 0);
  regs->reset();
  draw = true;
  step = 15;
}

void Chip8::cycle() {
  auto &pc = regs->pc;
  auto &sound_timer = regs->st;
  auto &delay_timer = regs->dt;
  std::uint16_t opcode = memory[pc] << 8u | memory[pc + 1];
  pc += 2;
  std::size_t x = (opcode & 0x0F00u) >> 8u;
  std::size_t y = (opcode & 0x00F0u) >> 4u;
  std::size_t n = opcode & 0xFu;
  std::uint8_t nn = opcode & 0xFFu;
  std::uint16_t nnn = opcode & 0xFFFu;

  auto upper_nibble = opcode >> 12u;

  switch (upper_nibble) {
    case 0:
      n == 0 ? exec_00E0() : exec_00EE();
      break;
    case 1:
      exec_1NNN(nnn);
      break;
    case 2:
      exec_2NNN(nnn);
      break;
    case 3:
      exec_3XNN(x, nn);
      break;
    case 4:
      exec_4XNN(x, nn);
      break;
    case 5:
      exec_5XY0(x, y);
      break;
    case 6:
      exec_6XNN(x, nn);
      break;
    case 7:
      exec_7XNN(x, nn);
      break;
    case 8:
      switch (n) {
        case 0:
          exec_8XY0(x, y);
          break;
        case 1:
          exec_8XY1(x, y);
          break;
        case 2:
          exec_8XY2(x, y);
          break;
        case 3:
          exec_8XY3(x, y);
          break;
        case 4:
          exec_8XY4(x, y);
          break;
        case 5:
          exec_8XY5(x, y);
          break;
        case 6:
          exec_8XY6(x);
          break;
        case 7:
          exec_8XY7(x, y);
          break;
        case 0xE:
          exec_8XYE(x);
          break;
        default:
          break;
      }
      break;
    case 9:
      exec_9XY0(x, y);
      break;
    case 0xA:
      exec_ANNN(nnn);
      break;
    case 0xB:
      exec_BNNN(nnn);
      break;
    case 0xC:
      exec_CXNN(x, nn);
      break;
    case 0xD:
      exec_DXYN(x, y, n);
      break;
    case 0xE:
      n == 1 ? exec_EXA1(x) : exec_EX9E(x);
      break;
    case 0xF:
      switch (n) {
        case 3:
          exec_FX33(x);
          break;
        case 5:
          switch (y) {
            case 1:
              exec_FX15(x);
              break;
            case 5:
              exec_FX55(x);
              break;
            case 6:
              exec_FX65(x);
              break;
            default:
              break;
          }
          break;
        case 7:
          exec_FX07(x);
          break;
        case 8:
          exec_FX18(x);
          break;
        case 9:
          exec_FX29(x);
          break;
        case 0xA:
          exec_FX0A(x);
          break;
        case 0xE:
          exec_FX1E(x);
          break;
        default:
          break;
      }
      break;
    default:
      std::cerr << "Undefined opcode!" << std::endl;
      std::cerr << "Opcode: " << std::hex << std::setw(4) << opcode
                << std::endl;
      prg_exit();
  }

  delay_timer -= delay_timer > 0 ? 1 : 0;
  sound_timer -= sound_timer > 0 ? 1 : 0;
}

void Chip8::exec_00E0() {
  std::fill(std::begin(display), std::end(display), 0);
}

void Chip8::exec_00EE() { regs->pc = regs->stack.at(regs->sp--); }

void Chip8::exec_1NNN(std::uint16_t nnn) { regs->pc = nnn; }

void Chip8::exec_2NNN(std::uint16_t nnn) {
  regs->stack.at(++regs->sp) = regs->pc;
  regs->pc = nnn;
}

void Chip8::exec_3XNN(std::size_t x, std::uint8_t nn) {
  if (regs->v[x] == nn) {
    regs->pc += 2;
  }
}

void Chip8::exec_4XNN(std::size_t x, std::uint8_t nn) {
  if (regs->v[x] != nn) {
    regs->pc += 2;
  }
}

void Chip8::exec_5XY0(std::size_t x, std::size_t y) {
  if (regs->v[x] == regs->v[y]) {
    regs->pc += 2;
  }
}

void Chip8::exec_6XNN(std::size_t x, std::uint8_t nn) { regs->v[x] = nn; }

void Chip8::exec_7XNN(std::size_t x, std::uint8_t nn) { regs->v[x] += nn; }

void Chip8::exec_8XY0(std::size_t x, std::size_t y) { regs->v[x] = regs->v[y]; }

void Chip8::exec_8XY1(std::size_t x, std::size_t y) {
  regs->v[x] |= regs->v[y];
}

void Chip8::exec_8XY2(std::size_t x, std::size_t y) {
  regs->v[x] &= regs->v[y];
}

void Chip8::exec_8XY3(std::size_t x, std::size_t y) {
  regs->v[x] ^= regs->v[y];
}

void Chip8::exec_8XY4(std::size_t x, std::size_t y) {
  auto &vx = regs->v[x];
  auto &vy = regs->v[y];
  std::uint16_t result =
      static_cast<std::uint16_t>(vx) + static_cast<std::uint16_t>(vy);
  regs->v[0xF] = (result > 255u) ? 1 : 0;
  vx = static_cast<std::uint8_t>(result);
}

void Chip8::exec_8XY5(std::size_t x, std::size_t y) {
  auto &vx = regs->v[x];
  const auto &vy = regs->v[y];
  regs->v[0xF] = (vx > vy) ? 1 : 0;
  vx -= vy;
}

void Chip8::exec_8XY6(std::size_t x) {
  std::uint8_t lsb = regs->v[x] & 0x1u;
  regs->v[0xF] = lsb;
  regs->v[x] >>= 1u;
}

void Chip8::exec_8XY7(std::size_t x, std::size_t y) {
  auto &vx = regs->v[x];
  const auto &vy = regs->v[y];
  regs->v[0xF] = (vy > vx) ? 1 : 0;

  vx = vy - vx;
}

void Chip8::exec_8XYE(std::size_t x) {
  std::uint8_t msb = regs->v[x] & 0x80u;
  regs->v[0xF] = (msb >> 7u);
  regs->v[x] <<= 1u;
}

void Chip8::exec_9XY0(std::size_t x, std::size_t y) {
  if (regs->v[x] != regs->v[y]) {
    regs->pc += 2;
  }
}

void Chip8::exec_ANNN(std::uint16_t nnn) { regs->i = nnn; }

void Chip8::exec_BNNN(std::uint16_t nnn) { regs->pc = nnn + regs->v[0]; }

void Chip8::exec_CXNN(std::size_t x, std::uint8_t nn) {
  std::uint8_t num = rand() % 256u;
  regs->v[x] = num & nn;
}

void Chip8::exec_DXYN(std::size_t x, std::size_t y, std::size_t n) {
  // The register VF will be used to determine if hit collision
  // occured or not. At first, no hit collision has occured,
  // so VF will have 0.
  auto &flag = regs->v[0xF];
  const auto &vx = regs->v[x];
  const auto &vy = regs->v[y];
  const auto &index = static_cast<std::size_t>(regs->i);

  flag = 0x0;
  std::size_t &height = n;
  std::uint8_t pixel{};

  // All sprites are 8 pixels wide and anywhere from 1-15 pixels
  // in height. The first for loop iterates through each line of
  // the sprite, and the inner for loop iterates through pixel in
  // the line.
  for (std::size_t row = 0; row < height; ++row) {
    pixel = memory[index + row];
    for (std::size_t bit = 0; bit < 8; ++bit) {
      if ((pixel & (0x80u >> bit))) {
        auto &draw_index = display[((vx + bit) + (vy + row) * WIDTH) % 2048UL];
        if (draw_index) flag = 0x1u;
        draw_index ^= UINT32_MAX;
      }
    }
  }

  draw = true;
}

void Chip8::exec_EX9E(std::size_t x) {
  const auto &vx = static_cast<std::size_t>(regs->v[x]);
  if (keypad[vx]) regs->pc += 2;
}

void Chip8::exec_EXA1(std::size_t x) {
  const auto &vx = static_cast<std::size_t>(regs->v[x]);
  if (!keypad[vx]) regs->pc += 2;
}

void Chip8::exec_FX07(std::size_t x) { regs->v[x] = regs->dt; }

void Chip8::exec_FX0A(std::size_t x) {
  bool key_pressed = false;
  for (int i = 0; i < 16; i++) {
    if (keypad[i]) {
      regs->v[x] = i;
      key_pressed = true;
    }
  }

  if (key_pressed == false) regs->pc -= 2;
}

void Chip8::exec_FX15(std::size_t x) { regs->dt = regs->v[x]; }

void Chip8::exec_FX18(std::size_t x) { regs->st = regs->v[x]; }

void Chip8::exec_FX1E(std::size_t x) {
  auto &flag = regs->v[0xF];
  auto &index = regs->i;
  auto &vx = regs->v[x];
  flag = (index + static_cast<std::uint16_t>(vx)) > 0xFFFu ? 0x1u : 0;
  index += static_cast<std::uint16_t>(vx);
}

void Chip8::exec_FX29(std::size_t x) {
  regs->i = 5 * (static_cast<std::uint16_t>(regs->v[x]));
}

void Chip8::exec_FX33(std::size_t x) {
  const auto &index = static_cast<std::size_t>(regs->i);
  memory[index] = (regs->v[x] % 1000) / 100;
  memory[index + 1] = (regs->v[x] % 100) / 10;
  memory[index + 2] = regs->v[x] % 10;
}

void Chip8::exec_FX55(std::size_t x) {
  auto &v_regs = regs->v;
  const auto &index = static_cast<std::size_t>(regs->i);
  for (std::size_t i = 0; i <= x; i++) {
    memory[index + i] = v_regs[i];
  }
}

void Chip8::exec_FX65(std::size_t x) {
  auto &v_regs = regs->v;
  const auto &index = static_cast<std::size_t>(regs->i);
  for (std::size_t i = 0; i <= x; i++) {
    v_regs[i] = memory[index + i];
  }
}
