#include "../include/display.hpp"

#include <array>
#include <cstdlib>
#include <iostream>

#ifdef __EMSCRIPTEN__
#include <emscripten/emscripten.h>
#endif

static inline void print_sdl_error(std::string type) {
  std::cerr << "Error initializing " << type << "! " << SDL_GetError()
            << std::endl;
}

Display::Display() {
  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
    print_sdl_error("SDL");
    goto exit;
  }

  window = SDL_CreateWindow("bCHIP_8", SDL_WINDOWPOS_UNDEFINED,
                            SDL_WINDOWPOS_UNDEFINED, SCALE * WINDOW_WIDTH,
                            SCALE * WINDOW_HEIGHT, SDL_WINDOW_SHOWN);

  if (!window) {
    print_sdl_error("window");
    goto quit_sdl;
  }

  renderer = SDL_CreateRenderer(
      window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

  if (!renderer) {
    print_sdl_error("renderer");
    goto release_window;
  }

  texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA8888,
                              SDL_TEXTUREACCESS_STREAMING, WINDOW_WIDTH,
                              WINDOW_HEIGHT);

  if (!texture) {
    print_sdl_error("texture");
    goto release_renderer;
  }

  return;

release_renderer:
  SDL_DestroyRenderer(renderer);
release_window:
  SDL_DestroyWindow(window);
quit_sdl:
  SDL_Quit();
exit:
#ifdef __EMSCRIPTEN__
  emscripten_force_exit(EXIT_FAILURE);
#else
  exit(EXIT_FAILURE);
#endif
}

Display::~Display() {
  SDL_DestroyTexture(texture);
  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);
  SDL_Quit();
}

void Display::draw(
    std::array<std::uint32_t, WINDOW_HEIGHT * WINDOW_WIDTH> &screen) {
  SDL_UpdateTexture(texture, nullptr, std::data(screen),
                    WINDOW_WIDTH * sizeof(uint32_t));

  SDL_RenderClear(renderer);
  SDL_RenderCopy(renderer, texture, nullptr, nullptr);
  SDL_RenderPresent(renderer);
}

int Display::key_press(std::array<uint8_t, 16> &keypad) {
  while (SDL_PollEvent(&event)) {
    if (event.type == SDL_QUIT) {
      return -1;
    }

    if (event.type == SDL_KEYDOWN) {
      switch (event.key.keysym.sym) {
        case SDLK_1:
          keypad[0] = 1;
          break;
        case SDLK_q:
          keypad[1] = 1;
          break;
        case SDLK_a:
          keypad[2] = 1;
          break;
        case SDLK_z:
          keypad[3] = 1;
          break;
        case SDLK_2:
          keypad[4] = 1;
          break;
        case SDLK_w:
          keypad[5] = 1;
          break;
        case SDLK_s:
          keypad[6] = 1;
          break;
        case SDLK_x:
          keypad[7] = 1;
          break;
        case SDLK_3:
          keypad[8] = 1;
          break;
        case SDLK_e:
          keypad[9] = 1;
          break;
        case SDLK_d:
          keypad[10] = 1;
          break;
        case SDLK_c:
          keypad[11] = 1;
          break;
        case SDLK_4:
          keypad[12] = 1;
          break;
        case SDLK_r:
          keypad[13] = 1;
          break;
        case SDLK_f:
          keypad[14] = 1;
          break;
        case SDLK_v:
          keypad[15] = 1;
          break;
        case SDLK_ESCAPE:
          return -1;
        default:
          break;
      }
    } else if (event.type == SDL_KEYUP) {
      switch (event.key.keysym.sym) {
        case SDLK_1:
          keypad[0] = 0;
          break;
        case SDLK_q:
          keypad[1] = 0;
          break;
        case SDLK_a:
          keypad[2] = 0;
          break;
        case SDLK_z:
          keypad[3] = 0;
          break;
        case SDLK_2:
          keypad[4] = 0;
          break;
        case SDLK_w:
          keypad[5] = 0;
          break;
        case SDLK_s:
          keypad[6] = 0;
          break;
        case SDLK_x:
          keypad[7] = 0;
          break;
        case SDLK_3:
          keypad[8] = 0;
          break;
        case SDLK_e:
          keypad[9] = 0;
          break;
        case SDLK_d:
          keypad[10] = 0;
          break;
        case SDLK_c:
          keypad[11] = 0;
          break;
        case SDLK_4:
          keypad[12] = 0;
          break;
        case SDLK_r:
          keypad[13] = 0;
          break;
        case SDLK_f:
          keypad[14] = 0;
          break;
        case SDLK_v:
          keypad[15] = 0;
          break;
        default:
          break;
      }
    }
  }

  return 0;
}
