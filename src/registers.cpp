#include "../include/registers.hpp"

Registers::Registers() : v(), dt(), st(), pc(0x200), i(), stack(), sp() {}

Registers::~Registers() {}

void Registers::reset() {
  std::fill(std::begin(v), std::end(v), 0);
  dt = 0;
  st = 0;
  pc = 0x200;
  i = 0;
  std::fill(std::begin(stack), std::end(stack), 0);
  sp = 0;
}
