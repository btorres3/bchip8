#include <chrono>
#include <cstdlib>
#include <iostream>
#include <string>
#include <thread>

#include "../include/cpu.hpp"
#include "../include/display.hpp"

#ifdef __EMSCRIPTEN__
#include <emscripten/emscripten.h>
#endif

static Chip8 chip8;
static Display display;

constexpr int FPS = 60;
constexpr int kFrameTargetTime = 1000 / FPS;

#ifdef __EMSCRIPTEN__
extern "C" {
void set_step(int step) { chip8.set_step(step); }
void set_game(char* rom) {
  std::string game(rom);
  chip8.set_game(game);
}
}
#endif

static inline void game_loop() {
  std::uint32_t start_time = SDL_GetTicks();

  chip8.run();

  if (chip8.get_draw()) {
    chip8.set_draw(false);
    display.draw(chip8.display);
  }

#ifdef __EMSCRIPTEN__
  if (display.key_press(chip8.keypad) == -1) {
    emscripten_cancel_main_loop();
  }
#endif

  auto time_to_wait = kFrameTargetTime - (SDL_GetTicks() - start_time);
  if (kFrameTargetTime > (SDL_GetTicks() - start_time)) {
    SDL_Delay(time_to_wait);
  }
}

int main([[maybe_unused]] int argc, [[maybe_unused]] char* argv[]) {
#ifdef __EMSCRIPTEN__
  chip8.set_game("../roms/pong2.ch8");
  emscripten_set_main_loop(game_loop, -1, 1);
#else
  if (argc == 1) {
    std::cout << "Please specify a game!" << std::endl;
    exit(EXIT_FAILURE);
  }

  std::string game(argv[1]);
  chip8.set_game(game);

  while (display.key_press(chip8.keypad) != -1) {
    game_loop();
  }
#endif
  return 0;
}
